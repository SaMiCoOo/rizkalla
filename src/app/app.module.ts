import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { reducers } from './reducers';
import { environment } from '../environments/environment';
import { CartModule } from './cart/cart.module';
import { EgyptIconComponent } from './egypt-icon/egypt-icon.component';
import { HeaderComponent } from './header/header.component';
import { NavComponent } from './nav/nav.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { UserIconComponent } from './user-icon/user-icon.component';
import { AsideComponent } from './aside/aside.component';
import { ProductCardComponent } from './product-card/product-card.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { ProductService } from './products.service';
import { ProductListItemComponent } from './product-list-item/product-list-item.component';
import { FooterComponent } from './footer/footer.component';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { CartPopupComponent } from './cart-popup/cart-popup.component';
import { MaterialButtonComponent } from './material-button/material-button.component';
import { IconComponent } from './icon/icon.component';
import { FiltersComponent } from './filters/filters.component';
import { SliderComponent } from './slider/slider.component';
import { NouisliderModule } from 'ng2-nouislider';
import { CategoryAccordionComponent } from './category-accordion/category-accordion.component';

@NgModule({
  declarations: [
    AppComponent,
    EgyptIconComponent,
    HeaderComponent,
    NavComponent,
    SearchInputComponent,
    UserIconComponent,
    AsideComponent,
    ProductCardComponent,
    ProductListItemComponent,
    FooterComponent,
    CartPopupComponent,
    MaterialButtonComponent,
    IconComponent,
    FiltersComponent,
    SliderComponent,
    CategoryAccordionComponent,
  ],
  imports: [
    BrowserModule,
    LazyLoadImageModule,
    NouisliderModule,
    FormsModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.fbConfig),
    CartModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 50,
      logOnly: environment.production
    }),
  ],
  providers: [
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
