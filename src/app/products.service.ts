import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { Product } from './shared/Product';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/first';
import { ProductFilters, RangeFilter } from './shared/ProductFilters';
import { map, switchMap, tap, share, shareReplay, skip } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ProductService {
  private filters$: BehaviorSubject<ProductFilters>;
  products: Observable<Product[]>;

  constructor(private afs: AngularFirestore) {
    this.products = this.fetchProducts();
    this.filters$ = new BehaviorSubject(new ProductFilters());
  }


  private fetchProducts(): Observable<Product[]> {
    return this.afs.collection<Product>('products')
      .snapshotChanges()
      .map(actions => actions.map(action => action.payload.doc)
        .map(doc => {
          return { id: doc.id, ...doc.data() } as Product;
        })
      );
  }

  getCalculatedFiltersRange(): Observable<ProductFilters> {
    return this.products.first().pipe(
      map(products => {
        const calculatedFilters = new ProductFilters();
        calculatedFilters.price.max = this.getMax('price', products);
        calculatedFilters.price.min = this.getMin('price', products);
        calculatedFilters.size.max = this.getMax('size', products);
        calculatedFilters.size.min = this.getMin('size', products);
        return calculatedFilters;
      })
    );
  }

  private getMax = (attr: string, array: any[]) => Math.max.apply(Math, array.map(o => o[attr]));
  private getMin = (attr: string, array: any[]) => Math.min.apply(Math, array.map(o => o[attr]));

  getFilteredProducts() {
    return this.products.pipe(
      switchMap(products => this.filters$.pipe(
        this._filter(products)
      ))
    );
  }

  getTextFilteredProducts() {
    return this.products.pipe(
      switchMap(products => this.filters$.pipe(
        map(filters => products.filter(p => p.name.toLowerCase().includes(filters.text.toLowerCase())))
      ))
    );
  }
  filter(filters: ProductFilters) {
    this.filters$.next({ ...filters });
  }

  private _filter = (products: Product[]) => {
    return map<ProductFilters, Product[]>(filters => products.filter(p => {
      return (p.price >= filters.price.min)
        && (p.price <= filters.price.max)
        && (p.size >= filters.size.min)
        && (p.size <= filters.size.max)
        && (p.name.toLowerCase().includes(filters.text.toLowerCase()));
    }));
  }

}
