import { CartItem } from './CartItem';

export function isCartItem(x: any | string): x is string {
  return (<CartItem>x).count !== undefined;
}
