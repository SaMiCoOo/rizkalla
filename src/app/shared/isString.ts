export function isString(x: any | string): x is string {
  return typeof x === 'string';
}
