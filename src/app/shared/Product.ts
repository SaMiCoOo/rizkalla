export interface Product {
  id: string;
  name: string;
  price: number;
  size: number;
  img: string;
}
