export interface RangeFilter {
  min: number;
  max: number;
}
export interface ProductFiltersInterface {
  price: RangeFilter;
  size: RangeFilter;
  text: string;
}
export class ProductFilters {
  price: RangeFilter = { min: 0, max: 999999 };
  size: RangeFilter = { min: 0, max: 999999 };
  text = '';

  constructor(init?: ProductFiltersInterface) {
    if (init) {
      this.price = init.price;
      this.text = init.text;
      this.size = this.size;
    }
  }
}
