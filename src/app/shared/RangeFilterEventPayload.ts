import { RangeFilter } from './ProductFilters';

export interface RangeFilterEventPayload { f: RangeFilter; prop: string; }
