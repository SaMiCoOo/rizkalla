import { Component, OnInit, HostListener } from '@angular/core';
import { ProductService } from './products.service';
import { Observable } from 'rxjs/Observable';
import { Product } from './shared/Product';
import { ProductFilters } from './shared/ProductFilters';
import { RangeFilterEventPayload } from './shared/RangeFilterEventPayload';
import { isString } from './shared/isString';
import { publish, share } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from './reducers';
import { selectTotal, selectAll, selectCartTotalPrice } from './cart/cart.selectors';
import { CartItem } from './shared/CartItem';
import { Delete } from './cart/cart.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './grid.scss']
})
export class AppComponent implements OnInit {
  navbarOpen: boolean;
  cartOpen = false;
  filtersOpen: boolean;
  filters: ProductFilters;
  filters$: Observable<ProductFilters>;
  cartCounter: Observable<number>;
  displayPopup: boolean;
  products: Observable<Product[]>;
  cartItems$: Observable<CartItem[]>;
  cartPrice$: Observable<number>;

  @HostListener('click', ['$event'])
  onClick(event) {
    if (event.target.id !== 'cart-icon') {
      this.displayPopup = false;
    }
  }
  toggleNavBar() {
    this.navbarOpen = !this.navbarOpen;
    console.log(this.navbarOpen);
  }
  toggleFilters() {
    this.filtersOpen = !this.filtersOpen;
    console.log(this.filtersOpen);
  }
  onCartClick(event: Event) {
    event.stopPropagation();
    this.displayPopup = true;
  }
  constructor(
    private ps: ProductService,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.products = this.ps.getFilteredProducts();
    this.filters$ = this.ps.getCalculatedFiltersRange();
    this.filters$.first().subscribe(
      f => {
        this.filters = f;
        this.ps.filter(this.filters);
      }
    );

    this.cartCounter = this.store.select(selectTotal);
    this.displayPopup = false;
    this.cartItems$ = this.store.select(selectAll);
    this.cartPrice$ = this.store.select(selectCartTotalPrice);
  }

  handleFilter(x: RangeFilterEventPayload | string) {
    if (isString(x)) {
      this.filters.text = x;
    } else {
      if (x.prop === 'price') {
        this.filters.price = x.f;
      }
      if (x.prop === 'size') {
        this.filters.size = x.f;
      }
    }
    this.ps.filter(this.filters);
  }

  removeItem(id: string) {
    this.store.dispatch(new Delete(id));
  }
}
