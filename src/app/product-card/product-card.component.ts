import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../shared/Product';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import * as fromCart from '../cart/cart.actions';
import { ProductService } from '../products.service';
@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  constructor(private store: Store<AppState>) { }
  addToCart() {
    this.store.dispatch(new fromCart.Add(this.product));
  }
  ngOnInit() {
  }

}
