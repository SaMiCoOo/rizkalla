import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {
  @Output() cartClicked = new EventEmitter<Event>();
  constructor() { }

  onCartClick(event) {
    this.cartClicked.emit(event);
  }

}
