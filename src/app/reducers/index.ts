import { ActionReducerMap } from '@ngrx/store';
import { cartAdapter, cartReducer, CartState } from '../cart/cart.reducer';

export const reducers: ActionReducerMap<AppState> = {
  cart: cartReducer
};

export interface AppState {
  cart: CartState;
}
