import { Component, OnInit, Input, Renderer2, HostListener, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { CartItem } from '../shared/CartItem';
import { Observable } from 'rxjs/observable';
import { selectAll, selectCartTotalPrice } from '../cart/cart.selectors';
@Component({
  selector: 'app-cart-popup',
  templateUrl: './cart-popup.component.html',
  styleUrls: ['./cart-popup.component.scss']
})
export class CartPopupComponent implements OnInit, OnDestroy {
  items: Observable<CartItem[]>;
  cartTotalPrice: Observable<number>;
  @Output() close = new EventEmitter<null>();
  onBodyClick() {
    this.renderer.removeClass(document.body, 'noscroll');
    this.renderer.removeClass(document.body, 'overlayed');
  }
  onClose() {
    this.close.emit();
  }
  constructor(
    private store: Store<AppState>,
    private renderer: Renderer2
  ) { }

  stop(event: Event) {
    event.stopPropagation();
  }
  ngOnInit() {
    this.renderer.addClass(document.body, 'noscroll');
    this.renderer.addClass(document.body, 'overlayed');
    this.items = this.store.select(selectAll);
    this.cartTotalPrice = this.store.select(selectCartTotalPrice);
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'noscroll');
    this.renderer.removeClass(document.body, 'overlayed');
  }
}
