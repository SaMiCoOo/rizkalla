import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../shared/Product';
import { CartItem } from '../shared/CartItem';
import { isCartItem } from '../shared/isCartItem';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import * as fromCart from './../cart/cart.actions';
@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss']
})
export class ProductListItemComponent implements OnInit {
  @Input() product: Product | CartItem;
  productIsCartItem: boolean;


  ngOnInit() {
    this.productIsCartItem = isCartItem(this.product);
  }
  constructor(private store: Store<AppState>) { }
  removeItem() {
    this.store.dispatch(new fromCart.Delete(this.product.id));
  }
}
