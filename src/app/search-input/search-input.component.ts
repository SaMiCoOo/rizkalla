import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Product } from '../shared/Product';
import { ProductService } from '../products.service';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {
  searchText = '';
  matchingProducts: Observable<Product[]>;
  @Output() search = new EventEmitter<string>();

  constructor(private ps: ProductService) { }

  ngOnInit() {
    this.matchingProducts = this.ps.getTextFilteredProducts()
      .pipe(
        map(products => products.slice(0, 3))
      );
  }
  onInput() {
    this.search.emit(this.searchText);
  }

}
