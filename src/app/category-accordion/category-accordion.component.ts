import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-category-accordion',
  templateUrl: './category-accordion.component.html',
  styleUrls: ['./category-accordion.component.scss']
})
export class CategoryAccordionComponent implements OnInit {

  @Input() category: string;
  @Input() items: string[];
  active: boolean;
  constructor() { }

  ngOnInit() {
    this.active = false;
  }

}
