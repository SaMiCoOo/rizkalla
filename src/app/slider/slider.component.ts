import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { RangeFilter } from '../shared/ProductFilters';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  constructor() { }

  @Input() filter: RangeFilter;
  @Input() step: number;
  min: number;
  max: number;

  @Output() valueChanged = new EventEmitter<RangeFilter>();

  @ViewChild('minRange') minRange: ElementRef;
  @ViewChild('maxRange') maxRange: ElementRef;
  ngOnInit() {
    if (this.filter.min < this.filter.max) {
      this.min = this.filter.min;
      this.max = this.filter.max;
    } else {
      console.warn('app-slider min must be less than max.');
    }
    // console.log(this.minRange);
    // this.maxRange.nativeElement.addEventListener('focus', (() => {
    //   this.maxRange.nativeElement.style.width = ((this.max - this.filter.min) / (this.max - this.min)) * 100 + '%';
    //   this.minRange.nativeElement.style.width = 100 - ((this.max - this.filter.min) / (this.max - this.min)) * 100 + '%';
    // }).bind(this));
    // this.maxRange.nativeElement.addEventListener('mouseout' , (() => {
    //   this.maxRange.nativeElement.style.width = 'initial';
    //   this.minRange.nativeElement.style.width = 100 - ((this.max - this.filter.max) / (this.max - this.min)) * 100 + '%';
    // }).bind(this));
  }

  lowValueChanged(v: number) {
    this.valueChanged.emit({
      min: v,
      max: this.filter.max
    });
  }

  highValueChanged(v: number) {
    this.valueChanged.emit({
      min: this.filter.min,
      max: v
    });
  }

}
