import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EgyptIconComponent } from './egypt-icon.component';

describe('EgyptIconComponent', () => {
  let component: EgyptIconComponent;
  let fixture: ComponentFixture<EgyptIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EgyptIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EgyptIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
