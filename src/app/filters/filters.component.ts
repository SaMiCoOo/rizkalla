import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { RangeFilterEventPayload } from '../shared/RangeFilterEventPayload';
import { RangeFilter } from '../shared/ProductFilters';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Input() price: RangeFilter;
  @Input() size: RangeFilter;
  sizeRange: number[];
  priceRange: number[];
  sizeLimits: RangeFilter;
  priceLimits: RangeFilter;
  @Output() filter = new EventEmitter<RangeFilterEventPayload>();

  constructor() { }
  ngOnInit() {
    this.sizeRange = [this.size.min, this.size.max];
    this.priceRange = [this.price.min, this.price.max];
    this.sizeLimits = this.size;
    this.priceLimits = this.price;
  }
  onChange(e, prop: string) {
    this.filter.emit({ f: { min: e[0], max: e[1]}, prop });
  }

}
