import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RangeFilter } from '../shared/ProductFilters';
import { RangeFilterEventPayload } from '../shared/RangeFilterEventPayload';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent {
  @Input() price: RangeFilter;
  @Input() size: RangeFilter;
  @Output() filter = new EventEmitter<RangeFilterEventPayload>();

  constructor() { }
  starRange = Array(5).fill(0);
  bestSellersRange = Array(3).fill(0);

  onInput(prop: string) {
    this.filter.emit({ f: this[prop], prop });
  }

}
