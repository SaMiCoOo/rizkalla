import { Action } from '@ngrx/store';
import { Product } from '../shared/Product';
import { CartItem } from '../shared/CartItem';

export enum CartActionTypes {
  ADD = '[Cart] Add One',
  UPDATE = '[Cart] Update',
  DELETE = '[Cart] Delete',
}

export class Add implements Action {
  readonly type = CartActionTypes.ADD;
  constructor(public cartItem: Product | CartItem) { }
}

export class Update implements Action {
  readonly type = CartActionTypes.UPDATE;
  constructor(
    public id: string,
    public changes: Partial<CartItem>,
  ) { }
}

export class Delete implements Action {
  readonly type = CartActionTypes.DELETE;
  constructor(public id: string) { }
}

export type CartActions
  = Add
  | Update
  | Delete;
