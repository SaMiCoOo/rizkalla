
import * as actions from './cart.actions';
import { ActionReducer } from '@ngrx/store';
import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { CartItem } from '../shared/CartItem';

export const cartAdapter = createEntityAdapter<CartItem>();

export interface CartState extends EntityState<CartItem> { }

export const initialState: CartState = cartAdapter.getInitialState();


export function cartReducer(
  state: CartState = initialState,
  action: actions.CartActions) {

  switch (action.type) {
    case actions.CartActionTypes.ADD:
      if ('count' in action.cartItem) {
        return cartReducer(state, new actions.Update(
          action.cartItem.id,
          {
            count: state.entities[action.cartItem.id].count + 1
          }
        ));
      }
      const cartItem = action.cartItem as CartItem;
      cartItem.count = 1;
      return cartAdapter.addOne(action.cartItem, state);

    case actions.CartActionTypes.UPDATE:
      return cartAdapter.updateOne({
        id: action.id,
        changes: action.changes,
      }, state);

    case actions.CartActionTypes.DELETE:
      return cartAdapter.removeOne(action.id, state);

    default:
      return state;
  }

}
