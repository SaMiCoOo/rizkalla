import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartIconComponent } from './cart-icon/cart-icon.component';
import { StoreModule } from '@ngrx/store';
import { cartReducer } from './cart.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('cart', cartReducer)
  ],
  exports: [
    CartIconComponent
  ],
  declarations: [
    CartIconComponent
  ]
})
export class CartModule { }
