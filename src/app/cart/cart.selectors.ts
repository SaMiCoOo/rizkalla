import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartState, cartAdapter } from './cart.reducer';

export const getCartState = createFeatureSelector<CartState>('cart');

export const {
  selectAll,
  selectEntities,
  selectIds,
} = cartAdapter.getSelectors(getCartState);

export const selectTotal = createSelector(
  selectAll,
  (items => {
    let count = 0;
    items.forEach(i => count += i.count);
    return count;
  })
);

export const selectCartTotalPrice = createSelector(
  selectAll,
  (items => {
    let total = 0;
    items.forEach(i => total += i.price * i.count);
    return total;
  })
);
